// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btn = document.getElementById("btn");
/* Obtenemos el div que muestra el resultado y lo almacenamos en una variable llamada "resultado" */
var numRandom = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
// Añadimos el evento click a la variable "btn"
btn.addEventListener("click", function() {

  var min = parseInt(inputUno.value);
  var max = parseInt(inputDos.value);
  if (Number.isInteger(min) || Number.isInteger(max)) {
    numRandom.innerHTML = getRndInteger(min, max);
  }else{window.alert("Ingrese solo números enteros")}
});

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
