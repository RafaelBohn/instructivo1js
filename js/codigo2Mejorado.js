// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btn = document.getElementById("btn");
/* Obtenemos el div que muestra el resultado y lo almacenamos en una variable llamada "resultado" */
var resultado = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
// Añadimos el evento click a la variable "btn"
btn.addEventListener("click", function() {
  /* Obtenemos el valor de cada input accediendo a su atributo "value" */
  var n1 = inputUno.value;
  var n2 = inputDos.value;
  var combo = document.getElementById("operacion");
  var opc = combo.options[combo.selectedIndex].text
  /* Llamamos a una función que permite realizar la suma de los números y los mostramos al usuario" */
  if (Number.isInteger(parseFloat(n1)) && Number.isInteger(parseFloat(n2))) {
    if (opc == "+") {
      resultado.innerHTML = suma(n1, n2);
    } else if (opc == "-") {
      resultado.innerHTML = resta(n1, n2);
    } else if (opc == "x") {
      resultado.innerHTML = multiplicacion(n1, n2);
    } else if (opc == "/") {
      resultado.innerHTML = division(n1, n2);
    }
  }else {
    alert("Ingrese ambos valores (solo números enteros)")
  }
});
/* Función que retorna la suma de dos números */
function suma(n1, n2) {
  // Es necesario aplicar parseInt a cada número
  return parseInt(n1) + parseInt(n2);
}

function resta(n1, n2) {
  // Es necesario aplicar parseInt a cada número
  return parseInt(n1) - parseInt(n2);
}

function multiplicacion(n1, n2) {
  // Es necesario aplicar parseInt a cada número
  return parseInt(n1) * parseInt(n2);
}

function division(n1, n2) {
  if (n2 <= 0) {
    window.alert("No se puede dividir por cero")
  } else
    return parseInt(n1) / parseInt(n2);
}

function ShowSelected() {
  /* Para obtener el valor */
  var opc = document.getElementById("operacion").value;
  alert(opc);
  alert(selected);
}
